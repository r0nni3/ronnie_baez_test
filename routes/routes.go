package routes

import (
	"net/http"

	"github.com/gorilla/mux"
	"rabs.com.do/PureGoApi/controllers"
)

type Route struct {
	Name        string
	Methods     []string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Methods(route.Methods...).
			Path(route.Pattern).
			Name(route.Name).
			Handler(controllers.HandleRoute(route.HandlerFunc))
	}

	return router
}

var routes = Routes{
	Route{
		"Index",
		[]string{"GET"},
		"/",
		controllers.IndexHandler,
	},
	Route{
		"Login",
		[]string{"GET", "POST"},
		"/login",
		controllers.LoginHandler,
	},
	Route{
		"Logout",
		[]string{"GET"},
		"/logout",
		controllers.LogoutHandler,
	},
	Route{
		"SignIn",
		[]string{"POST"},
		"/signin",
		controllers.SigninHandler,
	},
	Route{
		"Profiles",
		[]string{"GET"},
		"/profile",
		controllers.ProfilesHandler,
	},
	Route{
		"Profile",
		[]string{"GET", "POST"},
		"/profile/{id:[0-9]+}",
		controllers.ProfileHandler,
	},
	Route{
		"Fill profile",
		[]string{"GET"},
		"/fill-profile",
		controllers.FillProfileHandler,
	},
	Route{
		"Forgot",
		[]string{"POST"},
		"/forgot",
		controllers.ForgotPassHandler,
	},
	Route{
		"Reset",
		[]string{"GET", "POST"},
		"/reset/{token:[a-zA-Z0-9]+}",
		controllers.ResetPassHandler,
	},
}
