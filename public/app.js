var GOTEST = (function($) {
    var self = this;
    var $signupModal = null;
    var $forgotModal = null;
    
    $(document).ready(function() {
        $signupModal = $('.ui.signup.modal');
        $forgotModal = $('.ui.forgot.modal');
        
        $('.ui.link').click(function (e) {
            e.preventDefault();
            if ( $(this).hasClass('signup') ) {
                $signupModal.modal('show');
            }

            if ( $(this).hasClass('forgot') ) {
                $forgotModal.modal('show');
            }
        });

        $('.ui.signup.modal').modal({
            onApprove: function(e) {
                $modal = $(this);
                $currentButton = $(e);
                $currentButton.addClass('disabled');
                $currentButton.addClass('loading');
                $form = $modal.find('.ui.form');
                payload = $form.serialize()
                $.post('/signin', payload, function(data) {
                    $message = $form.find('.ui.message');
                    if (data.error == true) {
                        $message.html(data.message);
                    } else if (data.error == false) {
                        $message.addClass('success')
                        $message.html(data.message);
                        setTimeout(function() { $modal.modal('hide') }, 1500);
                    } else {
                        $message = $form.find('.ui.message');
                        $message.addClass('info')
                        $message.html("Something weird happend");
                    }
                    $form.addClass('error');
                }).fail(function(error) {
                    $form.find('.ui.message').html(error.responseText);
                    $form.addClass('error');
                    console.log(error)
                }).always(function() {
                    $currentButton.removeClass('disabled');
                    $currentButton.removeClass('loading');
                });
                return false;
            },
            onDeny: function(e) {}
        });
        
        $('.ui.login.form').form({
            fields: {
                email: {
                    identifier  : 'user',
                    rules: [
                        {type: 'empty', prompt: 'Please enter your e-mail'},
                        {type: 'email', prompt:'Please enter a valid e-mail'}
                    ]
                },
                password: {
                    identifier  : 'password',
                    rules: [
                        {type: 'empty', prompt: 'Please enter your password'},
                        {type: 'length[6]', prompt: 'Your password must be at least 6 characters'}
                    ]
                }
            },
            onSuccess: function(event, fields) {
                event.preventDefault();
                var $currentButton = $(event.target);
                var $form = $('.ui.login.form');
                var payload = $form.serializeArray();
                $.post('/login', payload, function(data) {
                    if (data.error == true) {
                        $form.find('.ui.message').html(data.message);
                        $form.addClass('error');
                    } else if (data.error == false) {
                        console.log("Success: " + data.message);
                    } else {
                        console.log("Something weird happend")
                    }
                }).fail(function(error) {
                    console.log(error);
                    $form.addClass('error');
                    $form.find('.ui.message').append(error.responseText);
                }).always(function() {
                    $currentButton.removeClass('disabled');
                    $currentButton.removeClass('loading');
                    window.location.reload();
                });
            }
        });

        $('.ui.editprofile.form').form({
            fields: {
                fullname: {
                    identifier: 'Profile.0.Value',
                    rules: [
                        {type: 'empty',prompt: 'Please enter your Name'}
                    ]
                },
                address: {
                    identifier: 'Profile.1.Value',
                    rules: [
                        {type: 'empty', prompt: 'Please enter your address'}
                    ]
                },
                email: {
                    identifier: 'Profile.2.Value',
                    rules: [
                        {type: 'empty', prompt: 'Please enter your e-mail'},
                        {type: 'email', prompt: 'Please enter a valid e-mail'}
                    ]
                },
                phone: {
                    identifier: 'Profile.3.Value',
                    rules: [
                        {type: 'empty', prompt: 'Please enter your phone'}
                    ]
                },

            },
            onSuccess: function(event, fields) {
                event.preventDefault();
                var $form = $(this);
                ajaxUpdateProfile($form, function(data) {
                    console.log(data);
                    window.location.reload();
                });
            }
        });
        $('.ui.edit.button').click(function(e) {
            $('.ui.profile.modal').modal('show');
        });
        $('.ui.profile.modal').modal({
            onApprove: function(event) {
                var $form = $(this).find('.ui.editprofile.form');
                ajaxUpdateProfile($form, function(data) {
                    window.location.reload();
                });
                return false;
            },
            onDeny: function(e) {}
        });

        $('.ui.forgot.form').form({
            onSuccess: function(event) { event.preventDefault(); }
        });
        $forgotModal.modal({
            onApprove: function(event) {
                var $this = $(this);
                var $form = $this.find('.ui.forgot.form');
                var action = $form.attr('action');
                var payload = $form.serialize();
                ajaxDo(action, payload, function(data) {
                    console.log(data);
                    if (!data.error) {
                        $form.find('.ui.error.message').html('');
                        $form.find("input[type=text]").val("");
                        $form.removeClass('error');
                        $this.modal('hide');
                    } else if(data.error) {
                        $form.find('.ui.error.message').html(data.message);
                        $form.addClass('error');
                    } else {
                    }
                }, function(error) {
                    console.log(error);
                });
                return false;
            },
            onDeny: function(event) {
                var $form = $(this).find('.ui.forgot.form');
                $form.find('.ui.error.message').html('');
                $form.find("input[type=text]").val("");
                $form.removeClass('error');
            }
        });

        $('.ui.reset-password.form').form({
            fields: {
                resetPassword: {
                    identifier: 'resetPassword',
                    rules: [
                        {type: 'empty',prompt: 'Please enter your password'}
                    ]
                },
                confirmPassword: {
                    identifier: 'confirmPassword',
                    rules: [
                        {type: 'empty', prompt: 'Please enter confirmation'},
                        {type: 'match[resetPassword]', prompt: "Password don't match"}
                    ]
                },
            },
            onSuccess: function(event, fields) {
                event.preventDefault();
                var $form = $(this);
                var action = $form.attr('action');
                var payload = $form.serialize();
                ajaxDo(action, payload, function(data) { // Succes
                    console.log(data);
                    var $message = $form.find('.ui.message');
                    $form.addClass('error');
                    $message.html(data.message);
                    if (data.error) {
                        console.log(data.error);
                        return;
                    }
                    
                    $message.addClass('success');
                    setTimeout(function() { window.location = "/" }, 2000);
                }, function(error) { // Fail
                    var $message = $form.find('.ui.message');
                    $form.addClass('error');
                    $message.html(error.responseMessage);
                }, null);
            }
        });
    });
    
    function ajaxLogin(payload) {
        var formData = $('.ui.login.form').serializeArray()
        $.each(formData, function (index, input) {
            if (input.name === "user") {
                input.value = payload.emailAddress;
            }
            if (input.name === "password") {
                var headers = payload.apiStandardProfileRequest.headers;
                input.value = headers.url + "?" +
                              headers.values[0].name +"="+
                              headers.values[0].value;
            }
        });
        formData.push({name: "social", value: true});
        formData.push({name: "ProfileData.0.Key", value: "fullname"});
        formData.push({name: "ProfileData.0.Value", value: payload.firstName + " "+ payload.lastName});
        
        $.post('/login', formData, function(data) {
            if (data.error == true) {
                console.error("Error: " + data.message);
            } else if (data.error == false) {
                console.log("Success: " + data.message);
                window.location = '/profile'
            } else {
                console.log("Something weird happend")
            }
        }).fail(function(error) {
            console.log(error);
                IN.User.logout(function(e) {
                    console.log(e);
                });
        }).always(function(){
            window.location.reload();
        });
    }

    function ajaxUpdateProfile($form, cb) {
        var action = $form.attr('action');
        var payload = $form.serializeArray();
        
        ajaxDo(action, payload, function(data) {
            if (data.error == true) {
                console.error("Error: " + data.message);
            } else if (data.error == false) {
                console.log("Success: " + data.message);
                cb(data);
            } else {
                console.log("Something weird happend", data);
            }
        }, function(error) {
            console.log(error);
        });
    }

    function ajaxDo(action, payload, cbSuccess, cbFail, cbAlways) {
        $.post(action, payload, cbSuccess)
            .fail(cbFail)
            .always(cbAlways);
    }

    return {
        ajaxLogin: ajaxLogin
    }
})($);
