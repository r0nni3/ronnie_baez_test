package controllers

import (
	"net/http"
	"strconv"

	"github.com/gorilla/schema"
	"rabs.com.do/PureGoApi/models"
)

func LoginHandler(res http.ResponseWriter, req *http.Request) {
	// Handles the Get Request to show the login page html response
	if req.Method == "GET" {
		if isNew := checkSession(req); isNew {
			showPage(res, "./templates/index.html", nil)
			return
		}
		session, _ := getSession(req)
		userId := session.Values["user_id"].(int64)
		http.Redirect(res, req,
			"/profile/"+strconv.FormatInt(userId, 10), 302)
	}

	// Login intent received
	if req.Method == "POST" {
		// Gets login data from body request
		if err := req.ParseForm(); err != nil {
			internalError(res, err)
			return
		}

		decoder := schema.NewDecoder()
		var user models.User
		err := decoder.Decode(&user, req.PostForm)
		if err != nil {
			internalError(res, err)
			return
		}

		if user.Social {
			err = models.IsSocialNewUser(&user)
			if err == nil {
				// Social user doesn't exist, create it
				_, err2 := models.AddsNewUser(&user)
				if err2 != nil {
					internalError(res, err2)
					return
				}
			}
		} else {
			// Checks user exists on DB and Valid credentials
			err = models.UserExists(&user)
			if err != nil {
				internalError(res, err)
				return
			}
		}

		// Sets session and Ok response
		_ = setSession(res, req, &user)

		// Redirects to current profile
		http.Redirect(res, req,
			"/profile/"+strconv.FormatInt(user.Id, 10), 302)
	}
}

func LogoutHandler(res http.ResponseWriter, req *http.Request) {
	// Delete session
	deleteSession(res, req)

	// Redirects to login
	http.Redirect(res, req, "/login", 302)
}
