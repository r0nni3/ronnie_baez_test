package controllers

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/sessions"
	"gopkg.in/mailgun/mailgun-go.v1"
	"html/template"
	"log"
	"net/http"
	"os"
	"rabs.com.do/PureGoApi/models"
)

type message struct {
	Error   bool   `json:"error"`
	Message string `json:"message"`
}

type dataMessage struct {
	Data interface{} `json:"data"`
	message
}

var store = sessions.NewCookieStore([]byte("97a69bfd2e40de9bb03b02c03c3d5babec623b56fcdce675f3c013f8172db4a6"))

func HandleRoute(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json;charset=utf-8")

		// Here we check api_keys, authorization, authentication
		// rate limit and any other validation for the request
		// If valid request
		fn(w, r)
		// else
		// manage fault response
	}
}

func internalError(res http.ResponseWriter, err error) {
	log.Println(err)
	res.WriteHeader(http.StatusInternalServerError)
	res.Write([]byte("Error: " + err.Error()))
}

func sendResponse(res http.ResponseWriter, msg interface{}) {
	response, err := json.Marshal(msg)

	if err != nil {
		internalError(res, err)
		return
	}

	res.WriteHeader(http.StatusAccepted)
	res.Write(response)
}

func showPage(res http.ResponseWriter, tpl string, data interface{}) {
	// Sets Header for HTML content
	res.Header().Set("Content-Type", "text/html;charset=utf-8")

	// Loads and execute template template
	t, err := template.ParseFiles(tpl)
	if err != nil {
		log.Println(err.Error())
	}
	t.Execute(res, data)
}

func getSession(req *http.Request) (*sessions.Session, error) {
	session, err := store.Get(req, "gotest")
	if err != nil {
		log.Println(err.Error())
	}
	return session, err
}

func checkSession(req *http.Request) bool {
	session, _ := getSession(req)
	return session.IsNew
}

func setSession(res http.ResponseWriter, req *http.Request, user *models.User) *sessions.Session {
	session, _ := getSession(req)
	session.Values["logged"] = true
	session.Values["user_id"] = user.Id
	session.Save(req, res)
	return session
}

func deleteSession(res http.ResponseWriter, req *http.Request) {
	if notNew := !checkSession(req); notNew {
		session, _ := getSession(req)

		// Clears all session data
		delete(session.Values, "logged")
		delete(session.Values, "user_id")
		session.Options.MaxAge = -1
		_ = session.Save(req, res)
	}
}

func sendMail(from string, to string, subject string, body string) error {
	domain := os.Getenv("MAILGUN_DOMAIN")
	apiKey := os.Getenv("MAILGUN_APIKEY")
	publicKey := os.Getenv("MAILGUN_PUBKEY")

	if domain == "" || apiKey == "" || publicKey == "" {
		log.Printf("Mailgun ENV not setup")
		return errors.New("Mailgun ENV not setup")
	}

	mg := mailgun.NewMailgun(domain, apiKey, publicKey)
	msg := mailgun.NewMessage(
		from+" <info@mg.rabs.com.do>",
		subject,
		body,
		to)
	resp, id, err := mg.Send(msg)
	if err != nil {
		log.Println(err)
		return err
	}

	log.Printf("ID: %s Resp: %s\n", id, resp)
	return nil
}
