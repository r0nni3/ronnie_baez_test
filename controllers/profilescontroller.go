package controllers

import (
	"net/http"
	"strconv"
)

func ProfilesHandler(res http.ResponseWriter, req *http.Request) {
	// The session is newlly created or an error
	// either way not a valid session active
	if isNew := checkSession(req); isNew {
		http.Redirect(res, req, "/login/", 302)
		return
	}

	// There is a valid active session
	session, _ := getSession(req)
	userId := session.Values["user_id"].(int64)
	http.Redirect(res, req, "/profile/"+strconv.FormatInt(userId, 10), 302)
}
