package controllers

import (
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"log"
	"net/http"
	"rabs.com.do/PureGoApi/models"
	"strconv"
)

func ProfileHandler(res http.ResponseWriter, req *http.Request) {
	// Checks session
	if isNew := checkSession(req); isNew {
		http.Redirect(res, req, "/login/", 302)
		return
	}
	// Get logged user
	session, _ := getSession(req)
	sessionId := session.Values["user_id"].(int64)

	// Checks param id and session Id match
	// and redirects if necesary
	vars := mux.Vars(req)
	paramId, _ := strconv.ParseInt(vars["id"], 10, 64)
	if paramId != sessionId {
		http.Redirect(res, req, "/profile/", 302)
		return
	}

	// Checks if user exist on database
	user := models.GetUserById(sessionId)
	if user == nil {
		// Not a valid user, deletes session, redirects to login
		deleteSession(res, req)
		http.Redirect(res, req, "/login/", 302)
		return
	}

	if req.Method == "GET" {
		// Checks if user finished the profile
		if !user.SignDone {
			http.Redirect(res, req, "/fill-profile/", 302)
			return
		}
		// Everything ok show profile
		user.LoadProfile()
		showPage(res, "./templates/profile.html", user)
	}

	if req.Method == "POST" {
		// Gets update data from body request
		if err := req.ParseForm(); err != nil {
			internalError(res, err)
			return
		}

		decoder := schema.NewDecoder()
		var profile models.User
		err := decoder.Decode(&profile, req.PostForm)
		if err != nil {
			internalError(res, err)
			return
		}

		err = user.UpdateProfile(profile.ProfileData)
		if err != nil {
			log.Printf("%v", &profile)
			sendResponse(res, &message{true, err.Error()})
			return
		}

		user.SetRegistrationCompleted(true)

		sendResponse(res, &message{false, "User updated successfuly"})
	}
}

func FillProfileHandler(res http.ResponseWriter, req *http.Request) {
	if isNew := checkSession(req); isNew {
		http.Redirect(res, req, "/login/", 302)
		return
	}
	// Get logged user
	session, _ := getSession(req)
	sessionId := session.Values["user_id"].(int64)

	// Checks if user exist on database
	user := models.GetUserById(sessionId)
	if user == nil {
		// Not a valid user, deletes session, redirects to login
		deleteSession(res, req)
		http.Redirect(res, req, "/login/", 302)
		return
	}

	if user.SignDone {
		http.Redirect(res, req, "/profile", 302)
		return
	}

	showPage(res, "./templates/fill-profile.html", user)
}

func ForgotPassHandler(res http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		http.Redirect(res, req, "/login/", 302)
		return
	}

	if req.Method == "POST" {
		if err := req.ParseForm(); err != nil {
			internalError(res, err)
			return
		}

		decoder := schema.NewDecoder()
		var u models.User
		err := decoder.Decode(&u, req.PostForm)
		if err != nil {
			internalError(res, err)
			return
		}

		// If user doesn't exist or user registered with Linkedin,
		// send and error
		exist := u.CheckUserExists()
		if notExist := !exist; notExist {
			sendResponse(res, &message{true, "User not valid"})
			return
		}
		if u.Social {
			sendResponse(res, &message{true, "User not valid"})
			return
		}

		// Generates Password reset token and saves it on DB
		token, err := u.SetToken()
		if err != nil {
			sendResponse(res, &message{true, err.Error()})
			return
		}

		// Tries to send email
		err = sendMail("Ronnie_Baez_Test",
			u.User,
			"Password reset link",
			`Hello, here is your password reset link: 
http://gotest.rabs.com.do/reset/`+token)
		if err != nil {
			sendResponse(res, &message{true,
				"Something not right, try again later"})
			return
		}

		// Everything ok
		sendResponse(res, &message{false,
			"Check your email for more instructions"})
		return
	}
}

func ResetPassHandler(res http.ResponseWriter, req *http.Request) {
	// Checks param id and session Id match
	// and redirects if necesary
	vars := mux.Vars(req)
	token := vars["token"]
	if token == "" {
		http.Redirect(res, req, "/login", 302)
		return
	}

	// Verifies token
	userId, err := models.CheckToken(token)
	if err != nil {
		showPage(res, "./templates/pass-reset.html",
			&message{true, err.Error()})
		return
	}

	// If GET request show form
	if req.Method == "GET" {
		showPage(res, "./templates/pass-reset.html",
			&message{false, strconv.FormatInt(userId, 10)})
		return
	}

	// Update password for userid
	if req.Method == "POST" {
		if err := req.ParseForm(); err != nil {
			internalError(res, err)
			return
		}

		// Get Form fields
		formUserId, _ := strconv.ParseInt(
			req.PostFormValue("userId"), 10, 64)
		formNewPass := req.PostFormValue("resetPassword")

		// Verifies form id equals to corresponding token id
		if formUserId != userId {
			sendResponse(res, &message{true, "Invalid Action"})
			return
		}

		// Everything ok, update password
		err = models.UpdateUserPassword(userId, formNewPass, token)
		if err != nil {
			sendResponse(res, &message{true, err.Error()})
			return
		}

		// Password updated
		sendResponse(res, &message{false, "Password update success!"})
		return
	}
}
