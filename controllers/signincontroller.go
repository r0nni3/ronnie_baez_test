package controllers

import (
	"net/http"

	"github.com/gorilla/schema"
	"rabs.com.do/PureGoApi/models"
)

func SigninHandler(res http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		// Gets signin data from body request
		if err := req.ParseForm(); err != nil {
			internalError(res, err)
			return
		}

		decoder := schema.NewDecoder()
		var u models.User
		if err := decoder.Decode(&u, req.PostForm); err != nil {
			internalError(res, err)
			return
		}

		// Adds user if new
		user, err := models.AddsNewUser(&u)
		if err != nil {
			// User exist, warn
			responseMsg := &message{Error: true,
				Message: "User exists"}
			sendResponse(res, responseMsg)
			return
		}

		// User created
		responseMsg := &dataMessage{
			user,
			message{Error: false,
				Message: "User created successfully"}}
		sendResponse(res, responseMsg)
	}
}
