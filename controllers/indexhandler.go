package controllers

import (
	"net/http"
)

func IndexHandler(res http.ResponseWriter, req *http.Request) {
	// The session is newlly created
	if checkSession(req) {
		http.Redirect(res, req, "/login", 302)
		return
	}

	// There is a valid active session
	http.Redirect(res, req, "/profile", 302)
}
