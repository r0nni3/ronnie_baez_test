package main

import (
	"log"
	"net/http"
	"os"

	"rabs.com.do/PureGoApi/routes"
)

func main() {
	// App routes
	router := routes.NewRouter()
	// Static Route
	router.PathPrefix("/static/").Handler(
		http.StripPrefix("/static/",
			http.FileServer(http.Dir("./public"))))

	app_port := os.Getenv("APP_PORT")
	err := http.ListenAndServe(":"+app_port, router)
	if err != nil {
		log.Fatal(err)
	}

	log.Print("Serving files...")
}
