package models

import (
	"crypto/hmac"
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

type ConnectionObj struct {
	Host     string
	User     string
	Password string
	Database string
}

var dbConnections map[string]*sql.DB
var dbSalt []byte

func init() {
	dbConnections = make(map[string]*sql.DB)
	_, _ = NewCon("default")
	dbSalt = []byte("g&p6cS!GW292@?VL")
}

func createConnectionObj() *ConnectionObj {
	user, pass, database, host, port := os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"), os.Getenv("DB_NAME"),
		os.Getenv("DB_HOST"), os.Getenv("DB_PORT")

	if user == "" {
		user = "root"
	}
	if pass == "" {
		pass = ""
	}
	if database == "" {
		database = ""
	}
	if host == "" {
		host = "localhost"
	}
	if port == "" {
		port = "3306"
	}

	return &ConnectionObj{
		User:     user,
		Password: pass,
		Database: database,
		Host:     host + ":" + port}
}

func (conObj *ConnectionObj) buildConnectionStr() string {
	return conObj.User + ":" + conObj.Password +
		"@tcp(" + conObj.Host + ")/" + conObj.Database +
		"?parseTime=true"
}

func (conObj *ConnectionObj) openConnection() (*sql.DB, error) {
	conString := conObj.buildConnectionStr()

	// Opens Connection
	con, err := sql.Open("mysql", conString)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	// Pings Database
	err = con.Ping()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	return con, nil
}

func NewCon(name string) (*sql.DB, error) {
	// If no connection name passed use "default"
	if name == "" {
		name = "default"
	}

	conObj := createConnectionObj()
	var err error
	dbConnections[name], err = conObj.openConnection()
	if err != nil {
		return nil, err
	}

	return dbConnections[name], nil
}

func GetCon(name string) (*sql.DB, bool) {
	// If no connection name passed use "default"
	if name == "" {
		name = "default"
	}

	_, found := dbConnections[name]
	return dbConnections[name], found
}

func CloseCon(name string) {
	if con, ok := GetCon(name); ok {
		_ = con.Close()
	}
}

func checkMAC(hexMessageMAC string, message, key []byte) bool {
	mac := hmac.New(sha256.New, key)
	mac.Write(message)
	expectedMAC := mac.Sum(nil)
	messageMAC, _ := hex.DecodeString(hexMessageMAC)
	return hmac.Equal(messageMAC, expectedMAC)
}

func createMAC(message, key []byte) string {
	mac := hmac.New(sha256.New, key)
	mac.Write(message)
	expectedMAC := mac.Sum(nil)
	return hex.EncodeToString(expectedMAC)
}

func createHash(message string) string {
	hasher := sha256.New()
	hasher.Write([]byte(message))
	return hex.EncodeToString(hasher.Sum(nil))
}
