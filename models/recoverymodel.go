package models

import (
	"errors"
	"log"
	"time"
)

type PassRecovery struct {
	Id        int64     `json:"id"`
	UserId    int64     `json:"user_id"`
	Token     string    `json:"token"`
	CreatedAt time.Time `json:"created_at"`
	ExpiresAt time.Time `json:"expires_at"`
}

// Checks token if hasn't expired and is valid return nil
// else return error
func CheckToken(token string) (int64, error) {
	con, ok := GetCon("default")
	if !ok {
		log.Println("No DB connection")
		return 0, errors.New("No DB Connection")
	}

	query := "SELECT *"
	query += " FROM pass_resets"
	query += " WHERE token=?"
	row := con.QueryRow(query, token)
	p := new(PassRecovery)
	err := row.Scan(&p.Id, &p.UserId, &p.Token, &p.CreatedAt, &p.ExpiresAt)
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}

	now := time.Now()
	if hasExpired := now.After(p.ExpiresAt); hasExpired {
		return 0, errors.New("Token Expired!")
	}

	return p.UserId, nil
}

// Creates new password recovery token for user
func (user *User) SetToken() (string, error) {
	con, ok := GetCon("default")
	if !ok {
		return "", errors.New("No DB Connection")
	}

	now := time.Now()
	expire := now.AddDate(0, 0, 1)
	token := createHash(user.User + ";" +
		now.String() + ";" +
		expire.String())

	query := "INSERT INTO pass_resets"
	query += " (`user_id`, `token`, created_at, `expires_at`)"
	query += " VALUES (?, ?, ?, ?)"
	res, err := con.Exec(query, user.Id, token, now, expire)
	if err != nil {
		log.Println(err.Error())
		return "", err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return "", err
	}

	if id == 0 {
		return "", errors.New("No rows inserted")
	}

	return token, nil
}

// Expires used reset Token
func (user *User) ExpireToken(token string) error {
	con, ok := GetCon("default")
	if !ok {
		return errors.New("No DB connection")
	}

	// Updates user password
	epoc := time.Date(1970, 01, 01, 0, 0, 1, 0, time.UTC)
	query := "UPDATE pass_resets"
	query += " SET expires_at=?"
	query += " WHERE user_id=?"
	query += " and token=?"
	row, err := con.Exec(query, epoc, user.Id, token)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	if count, _ := row.RowsAffected(); count == 1 {
		return nil
	}

	return errors.New("Nothing to expire!")
}
