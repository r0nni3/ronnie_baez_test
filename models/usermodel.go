package models

import (
	"errors"
	"log"
	"time"
)

type User struct {
	Id          int64     `json:"id"`
	User        string    `json:"username"`
	Password    string    `json:"-"`
	Social      bool      `json:"social"`
	Active      bool      `json:"active"`
	SignDone    bool      `json:"-"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	ProfileData []*Profile
}

func IsSocialNewUser(user *User) error {
	u := GetUserByUsername(user.User)
	if u != nil && u.Social {
		*user = *u
		return errors.New("Social User exists")
	}

	return nil
}

func UserExists(user *User) error {
	u := GetUserByUsername(user.User)
	if u == nil {
		return errors.New("Not a valid account")
	}

	if !checkMAC(u.Password, []byte(user.Password), dbSalt) {
		return errors.New("Invalid credentials")
	}

	*user = *u
	return nil
}

func AddsNewUser(newUser *User) (*User, error) {
	u := GetUserByUsername(newUser.User)
	if u != nil {
		return nil, errors.New("Error: User exists")
	}

	con, ok := GetCon("default")
	if !ok {
		return nil, errors.New("No DB connection")
	}

	hashedPassword := createMAC([]byte(newUser.Password), dbSalt)
	query := "INSERT INTO users (username, password, social)"
	query += " VALUES (?, ?, ?)"
	result, err := con.Exec(query,
		newUser.User, hashedPassword, newUser.Social)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	newUser.Id, err = result.LastInsertId()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	newUser.Active = true
	newUser.Password = hashedPassword
	return newUser, nil
}

func GetUserById(id int64) *User {
	con, ok := GetCon("default")
	if !ok {
		log.Println("No DB connection")
		return nil
	}

	query := "SELECT * FROM users"
	query += " WHERE id=?"
	query += " and active=TRUE"
	row := con.QueryRow(query, id)

	u := new(User)
	err := row.Scan(&u.Id, &u.User, &u.Password, &u.Social,
		&u.Active, &u.SignDone, &u.CreatedAt, &u.UpdatedAt)
	if err != nil {
		log.Println(err.Error())
		return nil
	}

	return u
}

func GetUserByUsername(username string) *User {
	con, ok := GetCon("default")
	if !ok {
		log.Println("No DB connection")
		return nil
	}

	query := "SELECT * FROM users"
	query += " WHERE username=?"
	query += " and active=TRUE"
	row := con.QueryRow(query, username)

	u := new(User)
	err := row.Scan(&u.Id, &u.User, &u.Password, &u.Social,
		&u.Active, &u.SignDone, &u.CreatedAt, &u.UpdatedAt)
	if err != nil {
		log.Println(err.Error())
		return nil
	}

	return u
}

func (user *User) SetRegistrationCompleted(isComplete bool) {
	con, ok := GetCon("default")
	if !ok {
		log.Println("No update, no connection active")
		return
	}

	now := time.Now()
	query := "UPDATE users"
	query += " SET signdone=?"
	query += ",updated_at=?"
	query += " WHERE id=?"
	query += " AND signdone=FALSE"
	_, err := con.Exec(query, isComplete, now, user.Id)
	if err != nil {
		log.Println(err.Error())
		return
	}

	user.SignDone = isComplete
	user.UpdatedAt = now
}

func (user *User) CheckUserExists() bool {
	u, ok := GetUserByUsername(user.User), false
	if u != nil {
		*user = *u
		ok = true
	}
	return ok
}

func UpdateUserPassword(userId int64, newPass string, token string) error {
	user := GetUserById(userId)
	if user == nil {
		return errors.New("Invalid user")
	}

	con, ok := GetCon("default")
	if !ok {
		return errors.New("No DB connection")
	}

	// Updates user password
	now := time.Now()
	hashedPassword := createMAC([]byte(newPass), dbSalt)
	query := "UPDATE users"
	query += " SET password=?"
	query += " ,updated_at=?"
	query += " WHERE id=?"
	row, err := con.Exec(query, hashedPassword, now, userId)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	// Expires token
	err = user.ExpireToken(token)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	// User updated
	if count, _ := row.RowsAffected(); count == 1 {
		return nil
	}

	return errors.New("No updates done!")
}
