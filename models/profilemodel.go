package models

import (
	"errors"
	"log"
	"time"
)

type Profile struct {
	Id        int64     `json:"id"`
	UserId    int64     `json:"user_id"`
	Key       string    `json:"key"`
	Value     string    `json:"value"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (user *User) HasKey(key string) bool {
	con, ok := GetCon("default")
	if !ok {
		log.Println("No DB connection")
		return false
	}

	query := "SELECT *"
	query += " FROM user_metas"
	query += " WHERE user_id=?"
	query += " and key=?"
	row := con.QueryRow(query, user.Id, key)
	p := new(Profile)
	err := row.Scan(&p.Id, &p.UserId, &p.Key, &p.Value, &p.CreatedAt,
		&p.UpdatedAt)
	if err != nil {
		log.Println(err.Error())
		return false
	}

	return true
}

func (user *User) InsertKey(key, value string) error {
	con, ok := GetCon("default")
	if !ok {
		return errors.New("No DB Connection")
	}

	query := "INSERT INTO user_metas (`user_id`, `key`, `value`)"
	query += " VALUES (?, ?, ?)"
	res, err := con.Exec(query, user.Id, key, value)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return err
	}

	if id == 0 {
		return errors.New("No rows inserted")
	}

	return nil
}

func (user *User) UpdateKey(key, value string) error {
	con, ok := GetCon("default")
	if !ok {
		return errors.New("No DB Connection")
	}

	now := time.Now()
	query := "UPDATE user_metas"
	query += " SET `value`=? , `updated_at`=?"
	query += " WHERE `user_id`=? AND `key`=?"
	res, err := con.Exec(query, value, now, user.Id, key)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	count, err := res.RowsAffected()
	if err != nil {
		log.Println(err.Error())
		return err
	}
	if count < 1 {
		return errors.New("No rows updated")
	}

	return nil
}

func (user *User) DeleteKey(profile *Profile) error {
	return errors.New("Out of scope")
}

func (user *User) LoadProfile() error {
	con, ok := GetCon("default")
	if !ok {
		return errors.New("No DB Connection")
	}

	query := "SELECT *"
	query += " FROM user_metas"
	query += " WHERE user_id=?"
	rows, err := con.Query(query, user.Id)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	defer rows.Close()

	metas := make([]*Profile, 0, 4)
	for rows.Next() {
		p := new(Profile)
		err = rows.Scan(&p.Id, &p.UserId, &p.Key, &p.Value,
			&p.CreatedAt, &p.UpdatedAt)
		if err != nil {
			log.Println(err.Error())
			return err
		}

		metas = append(metas, p)
	}

	user.ProfileData = metas
	return nil
}

func (user *User) UpdateProfile(profileData []*Profile) error {
	for _, newProfile := range profileData {
		// if value is empty don't update
		if newProfile.Value == "" {
			continue
		}

		// Try update first
		failed := user.UpdateKey(newProfile.Key, newProfile.Value)
		if failed != nil {
			// Key doesn't exist and we add it
			failed = user.InsertKey(
				newProfile.Key, newProfile.Value)
			if failed != nil {
				// Something weird log error to check later
				// and abort
				log.Println(failed.Error())
				return failed
			}
		}
	}

	// Updated finished, load updated profile to user
	failed := user.LoadProfile()
	if failed != nil {
		log.Println(failed.Error())
		return failed
	}

	return nil
}
